variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}
variable "ssh_password" {}

module "install" {
  source        = "./modules/docker_install"
  ssh_host      = var.ssh_host
  ssh_key       = var.ssh_key
  ssh_user      = var.ssh_user
  ssh_password  = var.ssh_password
}

