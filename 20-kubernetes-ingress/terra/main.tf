variable "ns" {}
variable "port" {}
variable "img" {}
variable "lbl-app" {}
variable "lbl-env" {}
variable "path" {}


provider "kubernetes" {
 config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.ns
    labels = {
      env = var.lbl-env
    }
  }
}


resource "kubernetes_pod" "monpremierpod" {
  metadata {
    name = "webserver"
    labels = {
      App = var.lbl-app
      env = var.lbl-env
    }
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }

  spec {
    container {
      image = var.img
      name  = "mynginx"

      port {
        container_port = var.port
      }
    }
  }
}

resource "kubernetes_service" "monpremierservice" {
  metadata {
    name = "webserver-service"
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
  spec {
    selector = {
      App = kubernetes_pod.monpremierpod.metadata[0].labels.App
    }
    port {
      port = var.port
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress_v1" "monpremieringress" {
  metadata {
    name = "webserver-ingress"
     annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }

 spec {
      rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.monpremierservice.metadata[0].name
              port {
                number = var.port
              }
            }
          }

          path = var.path
        }
      }
    }
  }
}
output "app" {
  value = {
    namespace   = kubernetes_namespace.namespace.metadata[0].name
    name        = kubernetes_pod.monpremierpod.metadata[0].name
    container   = kubernetes_pod.monpremierpod.spec[0].container[0].name
    service     = kubernetes_service.monpremierservice.spec[0].port[0].node_port
  }
}

