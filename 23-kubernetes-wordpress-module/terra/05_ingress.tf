resource "kubernetes_ingress_v1" "wp-ingress" {
  metadata {
    name = "webserver-ingress"
     annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }

 spec {
      rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.wp-svc-wordpress.metadata[0].name
              port {
                number = var.wordpress-port
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}

