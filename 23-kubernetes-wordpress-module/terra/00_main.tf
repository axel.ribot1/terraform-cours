variable "mysql-dir" {}
variable "mysql-password" {}
variable "mysql-img" {}
variable "mysql-lbl" {}
variable "mysql-port" {}
variable "wordpress-dir" {}
variable "wordpress-port" {}
variable "wordpress-img" {}
variable "wordpress-host" {}
variable "wordpress-lbl" {}


provider "kubernetes" {

    config_path = "~/.kube/config"
}

resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pass"
  }
  data = {
    password = var.mysql-password
  }
}


