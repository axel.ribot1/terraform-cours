variable "mysql-password" {}
variable "mysql-img" {}
variable "mysql-dir" {}
variable "mysql-lbl" {}
variable "wordpress-port" {}
variable "wordpress-img" {}
variable "wordpress-host" {}
variable "wordpress-dir" {}
variable "wordpress-lbl" {}


provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_persistent_volume" "mysql-volume-axel" {
  metadata {
    name = "mysql-volume-axel"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    persistent_volume_source {
      host_path {
        path = var.mysql-dir
      }
    }
  }
}

resource "kubernetes_persistent_volume" "wordpress-volume-axel" {
  metadata {
    name = "wordpress-volume-axel"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    persistent_volume_source {
      host_path {
        path = var.wordpress-dir
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "mysql-volume-claim-axel" {
  metadata {
    name = "mysql-volume-claim-axel"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.mysql-volume-axel.metadata[0].name
  }
}

resource "kubernetes_persistent_volume_claim" "wordpress-volume-claim-axel" {
  metadata {
    name = "wordpress-volume-claim-axel"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.wordpress-volume-axel.metadata[0].name
  }
}

resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pass"
  }
  data = {
    password = var.mysql-password
  }
}

resource "kubernetes_deployment" "mysql-deployment-axel" {
  metadata {
    name = "mysql-deployment-axel"
    labels = {
      app = var.mysql-lbl
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.mysql-lbl
      }
    }

    template {
      metadata {
        labels = {
          app = var.mysql-lbl
        }
      }

      spec {
        container {
          image = var.mysql-img
          name  = "mysql-axel"
         env {
           name = "MYSQL_ROOT_PASSWORD"
           value_from {
             secret_key_ref {
               name = kubernetes_secret.mysql.metadata.0.name
               key = "password"
             }
           }
          }
        env {
           name= "MYSQL_USER"
          value= "wpaxel"
         }
        env {
          name= "MYSQL_PASSWORD"
          value= "axel"
        }
       env {
          name= "MYSQL_DATABASE"
          value= "dbaxel"
        }
          port {
            container_port = 3306
            name = "mysql"
          }
        }
        }
      }
    }
  }

resource "kubernetes_service" "mysql-service-axel" {
  metadata {
    name = "mysql-service-axel"
  }
  spec {
    selector = {
      app = var.mysql-lbl
    }
    port {
      port = 3306
    }
  }
}


resource "kubernetes_deployment" "wordpress-deployment-axel" {
  metadata {
    name = "wordpress-deployment-axel"
    labels = {
      app = var.wordpress-lbl
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.wordpress-lbl
      }
    }

    template {
      metadata {
        labels = {
          app = var.wordpress-lbl
        }
      }

      spec {
        container {
          image = var.wordpress-img
          name  = "wordpress"
          env {
            name = "WORDPRESS_DB_HOST"
            value = "wp-mysql"
          }
          env {
            name = "WORDPRESS_DB_USER"
            value = "root"
            }
        env {
           name = "WORDPRESS_DB_PASSWORD"
           value_from {
             secret_key_ref {
               name = kubernetes_secret.mysql.metadata.0.name
               key = "password"
             }
           }
          }

        env {
           name = "WORDPRESS_DB_NAME"
           value = "dbaxel"
           }
           env {
            name = "WP_ALLOW_REPAIR"
            value = true
          }

          port {
            container_port = var.wordpress-port
            name = "wordpress"
          }
          volume_mount {
            name = "wordpress-persistent-storage"
            mount_path = "/var/www/html"
          }
        }
        volume {
          name = "wordpress-persistent-storage"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.wordpress-volume-claim-axel.metadata.0.name
          }
        }

      }
    }
  }
}


resource "kubernetes_service" "wp-svc-wordpress" {
  metadata {
    name = "wp-svc-wordpress"
  }
  spec {
    selector = {
      app = var.wordpress-lbl
    }
    port {
      port = var.wordpress-port
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress_v1" "wp-ingress" {
  metadata {
    name = "webserver-ingress"
     annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }

 spec {
      rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.wp-svc-wordpress.metadata[0].name
              port {
                number = var.wordpress-port
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}

