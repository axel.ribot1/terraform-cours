variable "mysql-password" {}
variable "mysql-dir" {}
variable "wordpress-dir" {}


provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_persistent_volume" "mysql-volume-axel" {
  metadata {
    name = "mysql-volume-axel"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    persistent_volume_source {
      host_path {
        path = var.mysql-dir
      }
    }
  }
}

resource "kubernetes_persistent_volume" "wordpress-volume-axel" {
  metadata {
    name = "wordpress-volume-axel"
  }
  spec {
    capacity = {
      storage = "2Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    persistent_volume_source {
      host_path {
        path = var.wordpress-dir
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "mysql-volume-claim-axel" {
  metadata {
    name = "mysql-volume-claim-axel"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.mysql-volume-axel.metadata[0].name
  }
 wait_until_bound = false
}

resource "kubernetes_persistent_volume_claim" "wordpress-volume-claim-axel" {
  metadata {
    name = "wordpress-volume-claim-axel"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "2Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.wordpress-volume-axel.metadata[0].name
  }
 wait_until_bound = false
}

resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-password-axel"
  }
  data = {
    password = var.mysql-password
  }
}


