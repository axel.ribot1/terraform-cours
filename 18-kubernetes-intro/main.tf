provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_pod" "nginx" {

  metadata {
    name = "nginx-example"
    labels = {
      App = "nginx"
    }
  }

  spec {
    container {
      image = "nginx:latest"
      name  = "mynginx"
      port {
        container_port = 80
      }
    }
  }

}
