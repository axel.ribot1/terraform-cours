#!/bin/bash

curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && sudo chmod +x minikube

sudo mv  minikube /usr/local/bin/

sudo apt -y  update

sudo apt install -y  apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

sudo apt -y update

sudo apt-cache policy docker-ce

sudo apt install -y docker-ce

sudo systemctl start docker

adduser ubuntu docker

sudo apt install conntrack

minikube start --driver=none


