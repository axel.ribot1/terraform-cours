resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = file(var.ssh_key)
  }
  provisioner "remote-exec" {
    inline = [
      "echo ${var.ssh_password} | sudo -S mkdir -p /srv/data/",
      "echo ${var.ssh_password} | sudo -S chmod 777 -R /srv/data/",
      "sleep 5s"
    ]
  }
}

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

provider "docker" {
  host = "tcp://${var.ssh_host}:2375"
}


resource "docker_volume" "docker_vol" {
  name = "myvol2"
  driver = "local"
  driver_opts = {
    o = "bind"
    type = "none"
    device = "/srv/data/"
  }
  depends_on = [ null_resource.ssh_target ]
}

resource "docker_network" "docker_net" {
  name = "mynet2"
}

resource "docker_image" "nginx" {
  name = "nginx:latest"
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "enginecks"
  ports {
    internal = 80
    external = 80
  }
  networks_advanced {
    name = docker_network.docker_net.name
  }
  volumes {
    volume_name = docker_volume.docker_vol.name
    container_path = "/usr/share/nginx/html/"
  }
}
