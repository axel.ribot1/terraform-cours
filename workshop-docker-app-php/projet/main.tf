variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}
variable "app_port" {}
variable "ssh_password" {}

module "docker_install" {
  source 	= "./modules/docker_install"
  ssh_host  	= var.ssh_host
  ssh_user  	= var.ssh_user
  ssh_key  	= var.ssh_key
  ssh_password  = var.ssh_password
}
module "docker_run" {
  source 	= "./modules/docker_run"
  ssh_host	= var.ssh_host
}
module "docker_app" {
  source 		= "./modules/docker_app"
  ssh_host		= var.ssh_host
  ssh_user 	  	= var.ssh_user
  ssh_key  		= var.ssh_key
  app_port      	= var.app_port
}

output "docker_ip_db" {
  value = module.docker_app.docker_ip_db
}
output "docker_ip_app" {
  value = module.docker_app.docker_ip_app
}
output "docker_volume" {
  value = module.docker_app.docker_volume
}
