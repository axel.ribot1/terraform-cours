output "docker_ip_db" {
  value = docker_container.db.ip_address
}
output "docker_ip_app" {
  value = module.docker_app.docker_ip_app
}
output "docker_volume" {
  value = docker_volume.db_data.driver_opts.device
}

